package com.test.stockprice.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PriceCalService {
    public int getMaxProfitForPurchase(Integer purchasePrice, List<Integer> stockPrices) {
        int maxProfit = 0;
        for (Integer price : stockPrices) {
            Integer gap = price > purchasePrice ? price - purchasePrice : 0;
            if (gap > maxProfit) {
                maxProfit = gap;
            }
        }

        return maxProfit;
    }

    public int getPossibleMaxProfitForTheDay(List<Integer> stockPrices) {
        List<Integer> possibleMaxProfits = new ArrayList<Integer>();
        if (stockPrices == null || stockPrices.isEmpty()) {
            return 0;
        }
        for (Integer index = 0; index < stockPrices.size(); index++) {
            List<Integer> pricesAfterThePoint = stockPrices.subList(index + 1, stockPrices.size());
            Integer possibleMaxProfit = getMaxProfitForPurchase(stockPrices.get(index), pricesAfterThePoint);
            possibleMaxProfits.add(index, possibleMaxProfit);
        }

        return Collections.max(possibleMaxProfits);
    }
}

