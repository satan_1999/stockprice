package com.test.stockprice;

import com.test.stockprice.service.PriceCalService;
import com.test.stockprice.util.InputUtil;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.List;

public class StockPriceApp {
    private static Logger log = Logger.getLogger(StockPriceApp.class);

    public static void main(String[] args) {
        BasicConfigurator.configure();
        log.setLevel(Level.DEBUG);
        //Take a integer list from user input
        List<Integer> stockPrices = InputUtil.takeInput();
        if (stockPrices == null || stockPrices.isEmpty()) {
            log.warn("StockPrices is empty");
            return;
        }
        //Create service class
        PriceCalService priceCalService = new PriceCalService();
        Integer maxProfit = priceCalService.getPossibleMaxProfitForTheDay(stockPrices);
        log.info("The max profit of the day is " + maxProfit);

    }

}
