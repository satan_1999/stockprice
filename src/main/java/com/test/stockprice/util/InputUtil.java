package com.test.stockprice.util;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class InputUtil {
    private static Logger log = Logger.getLogger(InputUtil.class);

    public static List<Integer> takeInput() {
        Scanner sc = new Scanner(System.in);
        List<Integer> stockPrices = new ArrayList<Integer>();

        boolean stop = false;
        do {
            log.info("Please enter integer, you can end the entering with a ';'");
            //for reading array
            String inputStr = sc.nextLine();
            if (inputStr.equalsIgnoreCase(";")) {
                stop = true;
                break;
            }
            try {
                stockPrices.add(Integer.parseInt(inputStr));
            } catch (Exception ex) {
                stop = true;
            }

        } while (stop != true);

        return stockPrices;
    }
}
