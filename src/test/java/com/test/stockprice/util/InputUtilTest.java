package com.test.stockprice.util;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import java.util.List;

import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

public class InputUtilTest {
    @Rule
    public final TextFromStandardInputStream systemInMock = emptyStandardInputStream();

    @Test
    public void testTakeInput_ShouldReturnListWith3ElementsInOrder_WhenUserEntered3Int() {
        //given
        systemInMock.provideLines("2", "10", "3", ";");
        //when
        List<Integer> result = InputUtil.takeInput();
        //then
        Assert.assertEquals(3, result.size());
        Assert.assertEquals(2, result.get(0).intValue());
        Assert.assertEquals(10, result.get(1).intValue());
        Assert.assertEquals(3, result.get(2).intValue());
    }

    @Test(expected = Test.None.class)
    public void testTakeInput_ShouldReturnListWithEmptyList_WhenUserEnterIsnotInteger() {
        //given
        systemInMock.provideLines("xx");
        //when
        List<Integer> result = InputUtil.takeInput();
        //then
        Assert.assertEquals(0, result.size());
    }
}
