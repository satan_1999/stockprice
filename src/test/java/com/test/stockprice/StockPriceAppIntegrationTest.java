package com.test.stockprice;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.LogManager;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.TestCase.assertEquals;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class StockPriceAppIntegrationTest {
    //This class is used to test the orchestration of all classes and flow.
    @Rule
    public final TextFromStandardInputStream systemInMock = emptyStandardInputStream();

    @Mock
    AppenderSkeleton appender;
    @Captor
    ArgumentCaptor<LoggingEvent> logCaptor;

    @Before
    public void setup() {
        LogManager.getLogger(StockPriceApp.class).addAppender(appender);
    }

    @Test(expected = Test.None.class)
    public void testGetMaxProfitForPurchase_ShouldReturnMaxProfit_6() throws Exception {
        //given
        systemInMock.provideLines("10", "7", "5", "8", "11", "9", ";");
        //when
        StockPriceApp.main(null);
        //then
        verify(appender, times(1)).doAppend(logCaptor.capture());
        assertEquals("The max profit of the day is 6", logCaptor.getValue().getRenderedMessage());
    }

    @Test(expected = Test.None.class)
    public void testGetMaxProfitForPurchase_ShouldReturnMaxProfit_0_IfThePriceIsDereasing() throws Exception {
        //given
        systemInMock.provideLines("20", "10", "5", "3", "2", "1", ";");
        //when
        StockPriceApp.main(null);
        //then
        verify(appender, times(1)).doAppend(logCaptor.capture());
        assertEquals("The max profit of the day is 0", logCaptor.getValue().getRenderedMessage());
    }

    @Test(expected = Test.None.class)
    public void testGetMaxProfitForPurchase_ShouldReturnMaxProfit_0_IfThePriceDoesnotChange() throws Exception {
        //given
        systemInMock.provideLines("20", "20", "20", ";");
        //when
        StockPriceApp.main(null);
        //then
        verify(appender, times(1)).doAppend(logCaptor.capture());
        assertEquals("The max profit of the day is 0", logCaptor.getValue().getRenderedMessage());
    }

    @Test(expected = Test.None.class)
    public void testGetMaxProfitForPurchase_ShouldReturnMaxProfit_0_IfThereIsOnlyOnePrice() throws Exception {
        //given
        systemInMock.provideLines("20", ";");
        //when
        StockPriceApp.main(null);
        //then
        verify(appender, times(1)).doAppend(logCaptor.capture());
        assertEquals("The max profit of the day is 0", logCaptor.getValue().getRenderedMessage());
    }

    @Test(expected = Test.None.class)
    public void testGetMaxProfitForPurchase_ShouldReturnGivenWarn_IfThereIsNoIntegerEntered() throws Exception {
        //given
        systemInMock.provideLines(";");
        //when
        StockPriceApp.main(null);
        //then
        verify(appender, times(1)).doAppend(logCaptor.capture());
        assertEquals("StockPrices is empty", logCaptor.getValue().getRenderedMessage());
    }

}
