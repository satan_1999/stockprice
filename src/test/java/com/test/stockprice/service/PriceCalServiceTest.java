package com.test.stockprice.service;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class PriceCalServiceTest {
    private PriceCalService service = new PriceCalService();

    @Test
    public void testGetMaxProfitForPurchase_ShouldReturnMaxProfit() throws Exception {
        //when
        Integer result = service.getMaxProfitForPurchase(10,
                Arrays.asList(1, 30, 3));
        //then
        Assert.assertEquals(20, result.intValue());
    }

    @Test
    public void testGetMaxProfitForPurchase_ShouldReturn0_IfStockPricesAreLessThanPurchasePrice() throws Exception {
        //when
        Integer result = service.getMaxProfitForPurchase(100,
                Arrays.asList(1, 30, 3));
        //then
        Assert.assertEquals(0, result.intValue());
    }

    @Test
    public void testGetMaxProfitForPurchase_ShouldReturn0_IfStockPricesIsEmpty() throws Exception {
        //when
        Integer result = service.getMaxProfitForPurchase(10,
                new ArrayList<Integer>());
        //then
        Assert.assertEquals(0, result.intValue());
    }

    @Test
    public void testGetPossibleMaxProfitForTheDay_ShouldReturn29() throws Exception {
        //when
        Integer result = service.getPossibleMaxProfitForTheDay(Arrays.asList(1, 30, 3));
        Assert.assertEquals(29, result.intValue());
    }

    @Test
    public void testGetPossibleMaxProfitForTheDay_ShouldReturnTheMaxProfit_56() throws Exception {
        //when
        Integer result = service.getPossibleMaxProfitForTheDay(Arrays.asList(1, 3, 30, 57));
        //then
        Assert.assertEquals(56, result.intValue());
    }

    @Test
    public void testGetPossibleMaxProfitForTheDay_ShouldReturnTheMaxProfit_47() throws Exception {
        //when
        Integer result = service.getPossibleMaxProfitForTheDay(Arrays.asList(4, 3, 30, 50));
        //then
        Assert.assertEquals(47, result.intValue());
    }

    @Test
    public void testGetPossibleMaxProfitForTheDay_ShouldReturnTheMaxProfit_54() throws Exception {
        //when
        Integer result = service.getPossibleMaxProfitForTheDay(Arrays.asList(100, 30, 3, 57));
        //then
        Assert.assertEquals(54, result.intValue());
    }

    @Test
    public void testGetPossibleMaxProfitForTheDay_ShouldReturn0_WhenThePriceIsDecreasing() throws Exception {
        Integer result = service.getPossibleMaxProfitForTheDay(Arrays.asList(100, 30, 23, 15));
        //then
        Assert.assertEquals(0, result.intValue());
    }

    @Test
    public void testGetPossibleMaxProfitForTheDay_ShouldReturn0_WhenThereIsOnlyOnePrice() throws Exception {
        //when
        Integer result = service.getPossibleMaxProfitForTheDay(Arrays.asList(100));
        //then
        Assert.assertEquals(0, result.intValue());
    }

    @Test
    public void testGetPossibleMaxProfitForTheDay_ShouldReturn0_WhenThereIsEmpty() throws Exception {
        //when
        Integer result = service.getPossibleMaxProfitForTheDay(new ArrayList<Integer>());
        //then
        Assert.assertEquals(0, result.intValue());
    }

    @Test
    public void testGetPossibleMaxProfitForTheDay_ShouldReturn0_WhenThereIsNull() throws Exception {
        //when
        Integer result = service.getPossibleMaxProfitForTheDay(null);
        //then
        Assert.assertEquals(0, result.intValue());
    }
}
